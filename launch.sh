# 1. installer docker (community edition)
# 2. créer un répertoire de travail 
# 3. remplacer $HOME/Code/nlp par le chemin vers le répertoire de travail choisi
# 4. lancer le script "bash launch.sh"



sudo docker run -it -p 8888:8888 \
	-v /home/Martin/Téléchargements/datamining:/root/sharedfolder \
	continuumio/anaconda3 /bin/bash \
	-c "pip install pycodestyle flake8 pycodestyle_magic &&
	jupyter lab --notebook-dir=/root/sharedfolder --ip='*' --port=8888 --no-browser --allow-root"

#sudo docker run -it -p 8888:8888 \
#	-v /home/Martin/Téléchargements/datamining:/root/sharedfolder \
#	continuumio/anaconda3 /bin/bash \
#	-c "jupyter lab --notebook-dir=/root/sharedfolder --ip='*' --port=8888 --no-browser --allow-root"

